package main

import(
	"fmt"
)

// why am I implementing fizzbuzz

func determineFizzBuzz(i int) bool {
	if (i % 3 == 0) && (i % 5 == 0){
		return true
	} else {
		return false
	}
}

func determineFizz(i int) bool {
	if (i % 3 == 0){
		return true
	} else {
		return false
	}
}

func determineBuzz(i int) bool {
	if (i % 5 == 0){
		return true
	} else {
		return false
	}
}


func main(){
	for i:=1; i<=100; i++ {
		if determineFizzBuzz(i){
			fmt.Println("FizzBuzz")
			continue
		}
		if determineFizz(i){
			fmt.Println("Fizz")
			continue
		}
		if determineBuzz(i){
			fmt.Println("Buzz")
			continue
		}
		fmt.Println(i)
	}
}
