#!/usr/bin/perl

# gather stats on return codes from web logs

use strict;
use warnings;
use Data::Dumper;

my $code;
my %codes;

while(<DATA>){
($code) = ($_ =~ (/.*(\d\d\d) \d+$/));
$codes{"total"}++;
$codes{$code}++;
}

for my $codekey (keys %codes){
  next if ($codekey eq "total");
  print "$codekey: count: $codes{$codekey}";
  print " % of total: " . $codes{$codekey} / $codes{"total"};
  print "\n";
}

__DATA__
64.242.88.10 - - [07/Mar/2004:16:10:02 -0800] "GET /api/post_tweet HTTP/1.1" 200 6291
64.242.88.10 - - [07/Mar/2004:16:10:23 -0800] "GET /api/request_car HTTP/1.1" 200 6291
64.242.88.10 - - [07/Mar/2004:16:10:34 -0800] "GET /api/request_car HTTP/1.1" 503 7352
64.242.88.10 - - [07/Mar/2004:16:11:01 -0800] "GET /api/post_tweet HTTP/1.1" 200 7352
64.242.88.10 - - [07/Mar/2004:16:11:12 -0800] "GET /api/send_mail HTTP/1.1" 200 6291
64.242.88.10 - - [07/Mar/2004:16:11:23 -0800] "GET /api/send_mail HTTP/1.1" 200 7352
64.242.88.10 - - [07/Mar/2004:16:12:03 -0800] "GET /api/send_mail HTTP/1.1" 200 7352
64.242.88.10 - - [07/Mar/2004:16:12:03 -0800] "GET /api/send_mail HTTP/1.1" 503 7352
