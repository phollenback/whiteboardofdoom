# How Far Am I From the International Space Station?

This program uses the google maps api and the open notify api to first figure out where on the earth you are, and then where the ISS is.  Then it calculates the number of miles between those two locations.

This also implements straight line distance calculation, although I'm unsure if I'm doing it right.

You need to have a valid google maps api key in the file spaceman.key.  You can get one of these at https://developers.google.com/maps/documentation/embed/get-api-key

I wrote this to become more proficient with using APIs in Go.