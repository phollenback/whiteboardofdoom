package main

import (
	"bufio"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"math"
	"net/http"
	"os"
	"strconv"
	"strings"

	geo "github.com/martinlindhe/google-geolocate"
)

// so this here program figures out how far you are from
// the international space station

// use http://open-notify.org/Open-Notify-API/ISS-Location-Now/
// to get the long/lat for the iss

// uses google maps api to get my location, so you have to put
// a valid google maps api key in a spaceman.key file in the
// same dir

// calculates both the distance between the two points on the earth's
// surface, and the straight line distance between you and the ISS,
// following
// https://math.stackexchange.com/questions/301410/how-to-calculate-distance-from-a-given-latitude-and-longitude-on-the-earth-to-a

// all these calculations assume the earth is a perfect sphere

// the iss position inside the iss data struct
type IssPosition struct {
	Latitude  string `json:"latitude"`
	Longitude string `json:"longitude"`
}

// json2go struct of the api data for the iss
type IssData struct {
	IssPosition IssPosition `json:"iss_position"`
	Message     string      `json:"message"`
	Timestamp   int         `json:"timestamp"`
}

// I define a location struct which is the lat and long of
// a given location.  Simpler to convert various formats in to
// one consistent one.
type Location struct {
	Lat  float64
	Long float64
}

// read in my google maps api key from a file
func readApiKey() (string, error) {
	file, err := os.Open("spaceman.key")
	if err != nil {
		return "", err
	}
	defer file.Close()

	scanner := bufio.NewScanner(file)
	// only one line so we don't need to scan in a loop
	scanner.Scan()
	if err := scanner.Err(); err != nil {
		return "", err
	}

	// welp we got the key if all went well eh?
	apiKey := scanner.Text()
	// we are good people who trim whitespace on input
	apiKey = strings.TrimSpace(apiKey)

	return apiKey, err
}

// call the google maps api to get my current location (lat/long)
func getMyLocation(apiKey string) (Location, error) {
	client := geo.NewGoogleGeo(apiKey)
	var myLocation Location

	res, err := client.Geolocate()
	if err != nil {
		return myLocation, err
	}

	// res is a geolocation Point struct - res.Lat and res.Long return
	// the lat and long

	// convert these to my consistent Location struct type
	// for ease of use
	myLocation.Lat = res.Lat
	myLocation.Long = res.Lng

	return myLocation, err
}

// call the open notify api to get the current location of the
// iss
func getIssLocation() (Location, error) {
	// ok so issData is the open api struct, and
	// issLocation is my simple lat/long struct.
	// the actual iss location is in issData.issPosition
	var issData IssData
	var issLocation Location

	// call the api
	res, err :=
		http.Get("http://api.open-notify.org/iss-now.json")
	if err != nil {
		return issLocation, err
	}

	// read the results from the api
	data, err := ioutil.ReadAll(res.Body)
	if err != nil {
		return issLocation, err
	}

	// convert the json into a struct
	err = json.Unmarshal(data, &issData)
	if err != nil {
		return issLocation, err
	}

	// convert issPosition json struct data to my Location
	// struct type
	issLocation.Lat, _ =
		strconv.ParseFloat(issData.IssPosition.Latitude, 64)
	issLocation.Long, _ =
		strconv.ParseFloat(issData.IssPosition.Longitude, 64)

	return issLocation, err
}

// from https://www.geodatasource.com/developers/go
// returns distance between two locations in statute miles
// assuming of course the earth is a perfect sphere, which is
// not true
func calcSurfaceDistance(myLocation Location, issLocation Location) float64 {
	const PI float64 = 3.141592653589793

	lat1 := myLocation.Lat
	lng1 := myLocation.Long
	lat2 := issLocation.Lat
	lng2 := issLocation.Long

	radlat1 := float64(PI * lat1 / 180)
	radlat2 := float64(PI * lat2 / 180)

	theta := float64(lng1 - lng2)
	radtheta := float64(PI * theta / 180)

	dist :=
		math.Sin(radlat1)*math.Sin(radlat2) +
			math.Cos(radlat1)*math.Cos(radlat2)*math.Cos(radtheta)

	if dist > 1 {
		dist = 1
	}

	dist = math.Acos(dist)
	dist = dist * 180 / PI
	dist = dist * 60 * 1.1515

	return dist
}

// calculate the straight line distance between my location and
// the ISS.  Assume that the ISS orbits at 400km - it actually varies
// as the orbit decays over time and the ISS gets re-boosted.
func calcStraightLineDist(myLocation Location, issLocation Location) float64 {
	myLat := float64(myLocation.Lat)

	// we care about the difference in longitude, not the sign
	longDiff := math.Abs(float64(myLocation.Long - issLocation.Long))

	// Re - radius of earth in km
	const Re = 6367.0
	// Rs - distance from center of earth to iss in km
	// ie assume iss orbits at 400km altitude
	const Rs = 6767.0

	// d^2 = (Rs − Re * cos longDiff * cos myLat)^2 +
	// (Re * sin longDiff * cos myLat)^2 +(Re sin longDiff)^2

	// dist - distance from me to iss, in km
	dSquared := (math.Pow((Rs-Re*math.Cos(longDiff)*math.Cos(myLat)), 2) +
		math.Pow((Re*math.Sin(longDiff)*math.Cos(myLat)), 2) +
		math.Pow((Re*math.Sin(longDiff)), 2))

	distInKm := math.Sqrt(dSquared)

	// look at us ugly americans
	distInMiles := distInKm * 0.6213712
	return distInMiles
}

// now that we have the two locations and the distance between
// them, print it all out
func printFunStuff(myLocation Location, issLocation Location,
	dist float64, straightLineDist float64) {

	// lets output some stuffs
	fmt.Printf("I'm at latitude %.4f", myLocation.Lat)
	fmt.Printf(" and longitude %.4f\n", myLocation.Long)
	fmt.Printf("The ISS is at latitude %.4f", issLocation.Lat)
	fmt.Printf(" and longitude %.4f\n", issLocation.Long)

	fmt.Print("The distance between those two points on earth is ")
	fmt.Printf("%.2f miles\n", dist)

	fmt.Print("The straight line distance between you and the ISS is ")
	fmt.Printf("%.2f miles\n", straightLineDist)
}

func main() {
	// get api key from file
	apiKey, err := readApiKey()
	if err != nil {
		log.Fatal(err)
	}

	// where am I?
	myLocation, err := getMyLocation(apiKey)
	if err != nil {
		log.Fatal(err)
	}

	// now that I know where I am, figure out where
	// the iss is
	issLocation, err := getIssLocation()
	if err != nil {
		log.Fatal(err)
	}

	// ok cool I now have two sets of coords - how far apart?
	earthDist := calcSurfaceDistance(myLocation, issLocation)

	straightLineDist := calcStraightLineDist(myLocation, issLocation)

	printFunStuff(myLocation, issLocation, earthDist, straightLineDist)
}
