package main

// postmates take home test, see
// https://gist.github.com/aabhassharma/774f5bc6e700b168974df3406a5acb68

import (
	"encoding/json"
	"io/ioutil"
	"log"
	"net/http"
	"os"
	"strconv"
)

type DeliveryStatus struct {
	CourierID int `json:"courier_id"`
	Location  struct {
		Latitude  float64 `json:"latitude"`
		Longitude float64 `json:"longitude"`
	} `json:"location"`
	Type string `json:"type"`
}

func processRequest(rw http.ResponseWriter, req *http.Request) {
	body, err := ioutil.ReadAll(req.Body)
	if err != nil {
		panic(err)
	}
	var deliveryStatus DeliveryStatus
	err = json.Unmarshal(body, &deliveryStatus)
	if err != nil {
		panic(err)
	}

	f, err := os.OpenFile("delivery.log", os.O_APPEND|os.O_CREATE|os.O_WRONLY, 0644)
	if err != nil {
		panic(err)
	}

	stringCourier := strconv.Itoa(deliveryStatus.CourierID)
	stringLat := strconv.FormatFloat(deliveryStatus.Location.Latitude, 'f', -1, 64)
	stringLong := strconv.FormatFloat(deliveryStatus.Location.Longitude, 'f', -1, 64)

	f.Write([]byte(stringCourier + " " + deliveryStatus.Type + " " +
		stringLat + " " +
		stringLong + "\n"))

	if err := f.Close(); err != nil {
		log.Fatal(err)
	}

}

func main() {

	http.HandleFunc("/deliver", processRequest)

	log.Fatal(http.ListenAndServe(":8081", nil))

}
