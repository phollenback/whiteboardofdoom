run program with: go run msg-logger-web.go

send messages to it with:

curl -X POST -H 'Content-Type: application/json' -d "{
  \"type\": \"delivery-started\",
  \"courier_id\": 1234,
  \"location\": {
    \"latitude\": 37.7749,
    \"longitude\": 122.4194
  }
}" localhost:8081/deliver
