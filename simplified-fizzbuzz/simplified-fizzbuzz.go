package main

// this was the wework coderpad interview question

import "fmt"

/* 
 count from 1..100 and replace multiples of 3 with `foo`
*/

func count(start int, end int) {
  for i := start; i <= end; i++ {
    if i % 3 == 0 {
      fmt.Println("foo")
    } else {
      fmt.Println(i)
    }
  }
}

func main() {

  count(1, 100)
}
