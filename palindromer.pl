#!/usr/bin/perl

use warnings;
use strict;

my $string = $ARGV[0];
my $strlength = 0;
my $beginning = "";
my $end = "";

print("checking if \"" . $string . "\" is a palindrome\n");

# remove all non alphanums from the string
$string =~ s/[^[:alnum:]]//g;

$strlength = length($string);

# cut string in half, reverse second half, compare.
# if string length is odd, include final char in both
# ie tacocat splits to "taco" and "ocat"
# while "foobboof" splits to "foob" and "boof"
if (($strlength % 2) == 0) {
    $beginning = substr($string, 0, $strlength/2);
} else {
    # capture that middle character in both
    $beginning = substr($string, 0, ($strlength/2)+1);
}
$end = substr($string, ($strlength/2));

if($beginning eq reverse($end)){
    print("it's a palindrome!\n");
} else {
    print("you lose!\n");
}
