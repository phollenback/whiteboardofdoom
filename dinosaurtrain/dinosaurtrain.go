package main

// this was one of the two facebook coderpad tests

import (
	"bufio"
	"fmt"
	"math"
	"os"
	"sort"
	"strconv"
	"strings"
)

/*
# You will be supplied with two data files in CSV format. The first file
# contains statistics about various dinosaurs. The second file contains
# additional data.
#
# Given the formula:
#
# speed = ((STRIDE_LENGTH / LEG_LENGTH) - 1) * SQRT(LEG_LENGTH * g)
#     where g = 9.8 m/s^2 (gravitational constant)
#
# write a program to read in the data files from disk, it must then print the
# names of only the bipedal (i.e. two-legged) dinosaurs from fastest to slowest. Do not print any
# other information.
#
# $ cat dataset1.csv
# NAME,LEG_LENGTH,DIET
# Hadrosaurus,1.2,herbivore
# Struthiomimus,0.92,omnivore
# Velociraptor,1.0,carnivore
# Triceratops,0.87,herbivore
# Euoplocephalus,1.6,herbivore
# Stegosaurus,1.40,herbivore
# Tyrannosaurus Rex,2.5,carnivore
#
#
# $ cat dataset2.csv
# NAME,STRIDE_LENGTH,STANCE
# Euoplocephalus,1.87,quadrupedal
# Stegosaurus,1.90,quadrupedal
# Tyrannosaurus Rex,5.76,bipedal
# Hadrosaurus,1.4,bipedal
# Deinonychus,1.21,bipedal
# Struthiomimus,1.34,bipedal
# Velociraptor,2.72,bipedal
*/

type Dinosaur struct {
	name         string
	legLength    float64
	strideLength float64
	bipedal      bool
	speed        float64
}

type Dinosaurs map[string]*Dinosaur

func readFirstCsv() Dinosaurs {
	dinos := make(Dinosaurs)

	setOneFile, err := os.Open("dataset1.csv")
	if err != nil {
		panic(err)
	}
	defer setOneFile.Close()
	scanner := bufio.NewScanner(setOneFile)
	// skip the first line as it's a header
	scanner.Scan()
	for scanner.Scan() {
		var dino Dinosaur
		fields := strings.Split(scanner.Text(), ",")
		dino.name = fields[0]
		dino.legLength, _ = strconv.ParseFloat(fields[1], 64)

		dinos[dino.name] = &dino
	}
	return dinos
}

func readSecondCsv(dinos Dinosaurs) Dinosaurs {
	setTwoFile, err := os.Open("dataset2.csv")
	if err != nil {
		panic(err)
	}
	defer setTwoFile.Close()

	scanner := bufio.NewScanner(setTwoFile)
	// skip header line
	scanner.Scan()
	for scanner.Scan() {
		var dino Dinosaur
		words := strings.Split(scanner.Text(), ",")
		dino.name = words[0]
		dino.strideLength, _ = strconv.ParseFloat(words[1], 64)
		if words[2] == "bipedal" {
			dino.bipedal = true
		}

		// check if we already have this dino
		if _, ok := dinos[dino.name]; ok {
			dinos[dino.name].strideLength =
				dino.strideLength
			dinos[dino.name].bipedal = dino.bipedal
			dino.legLength = dinos[dino.name].legLength
			dino.speed = ((dino.strideLength / dino.legLength) - 1) *
				math.Sqrt(dino.legLength*9.8)
			dinos[dino.name].speed = dino.speed
		}
	}
	return dinos
}

// print the names of the bipedal dinos from fastest to slowest
func printBipedalSpeeders(dinos Dinosaurs) {
	dinoSlice := []Dinosaur{}

	for _, dino := range dinos {
		if dino.bipedal == true {
			dinoSlice = append(dinoSlice, *dino)
		}
	}

	sort.Slice(dinoSlice, func(i, j int) bool {
		return dinoSlice[i].speed > dinoSlice[j].speed
	})

	for _, dino := range dinoSlice {
		fmt.Println(dino.name)
	}

}

func main() {
	dinos := readFirstCsv()
	dinos2 := readSecondCsv(dinos)
	printBipedalSpeeders(dinos2)
}
