package main

import (
	"bufio"
	"fmt"
	"os"
	"strings"
)

type depNode struct {
	name     string
	children []*depNode
}

var (
	depNodeMap    = map[string]*depNode{}
	installedPkgs = []string{}
)

// check if a package is already installed
func packageIsInstalled(pkg string) bool {
	for _, installedPkg := range installedPkgs {
		if installedPkg == pkg {
			return true
		}
	}
	return false
}

// remove the given package from the list of installed pkgs
func removePackageFromInstalledList(pkg string) {
	newList := []string{}
	for _, item := range installedPkgs {
		if item != pkg {
			newList = append(newList, pkg)
		}
	}
	installedPkgs = newList
}

func readInput() {
	scanner := bufio.NewScanner(os.Stdin)
	// first line is number of lines to process
	// (including the first line)
	scanner.Scan()
	// numCommands, _ := strconv.Atoi(scanner.Text())
	for scanner.Scan() {
		fmt.Println(scanner.Text())
		line := strings.Split(scanner.Text(), " ")
		action := line[0]
		switch action {
		case "DEPEND":
			processDepend(line[1], line[2:])
		case "INSTALL":
			processInstall(line[1])
		case "REMOVE":
			processRemove(line[1])
		case "LIST":
			processList()
		case "END":
			break
		default:
			fmt.Println("Ignoring invalid action " + action)
		}
	}
}

// process a DEPEND input line
func processDepend(parent string, deps []string) {
	var parentNode depNode
	parentNode.name = parent

	for _, dep := range deps {
		if checkForCircularDep(parent, dep) {
			fmt.Println(dep + " depends on " +
				parent + ", ignoring command")
		} else {
			var childNode depNode
			childNode.name = dep
			parentNode.children =
				append(parentNode.children, &childNode)
		}
	}

	// ok create the map entry for the parent
	depNodeMap[parent] = &parentNode
}

// make sure we don't have a circular dependency
// this initial inplementation doesn't recurse, just
// checks for direct dependencies.
func checkForCircularDep(parent string, dep string) bool {
	// see if dep is already in the depNodeMap as a parent
	// if it isn't, we're done
	depNode, ok := depNodeMap[dep]
	if !ok {
		// there isn't a dep map for the dep, we're fine
		return false
	} else {
		// dep already exists as a parent, make sure
		// parent doesn't exist as one of it's deps
		for _, child := range depNode.children {
			if child.name == parent {
				// we have a circular dep!
				return true
			}
		}
		// we didn't find a circular dep, all ok
		return false
	}
}

// attempt to install a package
func processInstall(pkg string) {
	// make sure pkg isn't already installed
	if packageIsInstalled(pkg) {
		fmt.Println(pkg + " is already installed")
		return
	}

	// check if there's a dep tree for the package
	parentNode, depsExist := depNodeMap[pkg]
	if depsExist {
		for _, dep := range walkDeps(parentNode.name) {
			fmt.Println("Installing " + dep)
			installedPkgs = append(installedPkgs, dep)
		}
		fmt.Println("Installing " + pkg)
		installedPkgs = append(installedPkgs, pkg)
	} else {
		fmt.Println("Installing " + pkg)
		installedPkgs = append(installedPkgs, pkg)
	}
}

// walk the dependency tree for package named pkg and return
// list of dependencies that need to be installed first
// returns list of dependencies to install, in order
// also check if a dep is already installed - if so, don't
// install again
func walkDeps(pkg string) []string {
	// initial implementation - just return immediate children
	// real implementation needs to reurse children
	parentNode := depNodeMap[pkg]
	pkgsToInstall := []string{}

	for _, child := range parentNode.children {
		// is child already installed?
		if !packageIsInstalled(child.name) {
			pkgsToInstall =
				append(pkgsToInstall, child.name)
		}
	}
	return pkgsToInstall
}

// attempt to remove a package
// don't allow removal if package is depended on by another pkg
func processRemove(pkg string) {
	if !packageIsInstalled(pkg) {
		return
	}
	if !checkIfStillNeeded(pkg, "") {
		// package is not needed as a dep to another
		// installed package, we are safe to continue
		fmt.Println("Removing " + pkg)
		// try to remove unused dependencies
		attemptAutoRemove(pkg)
		removePackageFromInstalledList(pkg)
		return
	} else {
		// can't remove package yet
		fmt.Println(pkg + " is still needed")
		return
	}
}

// check if we can autoremove deps - ie when we remove a package,
// does that make it safe to remove the package's dependencies?
func attemptAutoRemove(pkg string) {
	depNode, ok := depNodeMap[pkg]
	if !ok {
		// whoops what is going on
		panic("attempt to remove non-existent package")
	}
	for _, child := range depNode.children {
		// check each dep for the current package -
		// if it's not listed as a dependency for any
		// other packages, it's safe to auto remove
		if checkIfStillNeeded(child.name, pkg) {
			// we can't remove the pkg
		} else {
			fmt.Println("Removing " + pkg)
			removePackageFromInstalledList(pkg)
		}
	}
}

// check if a package is still needed as a dependency - ie
// it's listed as a dependency of an installed package.
// parent is used when we want to check that _only_ a
// certain parent still needs a package.
func checkIfStillNeeded(pkg string, parent string) bool {
	for _, parentNode := range depNodeMap {
		if parent == parentNode.name {
			// we passed a parent to exclude
			continue
		} else {
			for _, child := range parentNode.children {
				if child.name == pkg {
					// dep is still needed
					return true
				}
			}
		}
	}
	return false
}

// list installed packages
func processList() {
	for _, pkg := range installedPkgs {
		fmt.Println(pkg)
	}
}

func main() {
	readInput()

}
