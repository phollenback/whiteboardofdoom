package main

// salesforce in-person whiteboard test
import (
	"fmt"
	"math/rand"
	"time"
)

// create a singly linked list and reverse it

// each individual list node
type Node struct {
	val  int
	next *Node
}

// the entire list of nodes
type List struct {
	start *Node
}

// generate a random integer and return it
func genRandomInt() int {
	min := 1
	max := 999

	r := rand.New(rand.NewSource(time.Now().UnixNano()))
	return r.Intn(max-min) + min
}

// create a list node
func createNode() *Node {
	var node Node
	node.val = genRandomInt()
	return &node
}

func (list *List) insertNode(newNode *Node) {
	if list.start == nil {
		// this is a new list
		list.start = newNode
	} else {
		currentNode := list.start
		// traverse list until end
		for currentNode.next != nil {
			currentNode = currentNode.next
		}
		// now we are at end of list
		currentNode.next = newNode
	}
}

// reverse a linked list
func (list *List) reverseList() {
	// start at the beginning
	currentNode := list.start
	var prevNode, nextNode *Node

	for currentNode != nil {
		nextNode = currentNode.next
		currentNode.next = prevNode
		prevNode = currentNode
		currentNode = nextNode
	}
	list.start = prevNode
}

// create a singly linked list of length count.
// the value for each node is randomly generated
func createLinkedList(count int) *List {
	var myList List

	// loop to create rest of nodes
	for i := 0; i < count; i++ {
		myList.insertNode(createNode())
	}
	return &myList
}

// walk a list and print the values
func (list *List) printList() {
	currentNode := list.start
	for currentNode.next != nil {
		fmt.Println(currentNode.val)
		currentNode = currentNode.next
	}
	// gotta print that last value!
	fmt.Println(currentNode.val)
}

func main() {
	myList := createLinkedList(5)
	myList.printList()
	fmt.Println("reversed list:")
	myList.reverseList()
	myList.printList()
}
