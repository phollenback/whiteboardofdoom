package main

// scribd coderpad test

import (
	"fmt"
	"sort"
)

// # Given an amount of change to be given and a set of available
// coins, write a method that determines the best way (smallest
// number) to give change.
//
// Return empty array if change can’t be
// made.
//
// change(77) => [25, 25, 25, 1, 1]
//
// change(21, [25, 10, 5]) => []
//
// def change(amount, coins=[25, 10, 5, 1]) end
//
// [3, 0, 0, 2]
// [25, 25, 25, 1, 1]

// actually calculate the change
func calcChange(startsWith int, possibles []int) []int {
	var counts = []int{}
	remaining := startsWith

	// sort the list of change types from largest
	// to smallest, otherwise we could just give everyone
	// lots of pennies.
	sort.Slice(possibles, func(i, j int) bool {
		return i < j
	})
	for _, possible := range possibles {
		numTimes := remaining / possible
		remaining = remaining % possible

		counts = append(counts, numTimes)
	}

	if remaining != 0 {
		return ([]int{})
	}
	fmt.Println(possibles)
	result := translate(possibles, counts)
	return result
}

// need a translate func because calcChange gives us
// counts per change type, but we need to expand the
// count for each type of change
func translate(possibles []int, counts []int) []int {
	var result []int

	for i, possibile := range possibles {
		for j := 0; j < counts[i]; j++ {
			result = append(result, possibile)
		}
	}
	return result
}

func main() {
	changeAvailable := []int{25, 10, 5, 1}
	changeFor := 77
	result := calcChange(changeFor, changeAvailable)
	fmt.Println(changeFor)
	fmt.Println(result)
}
