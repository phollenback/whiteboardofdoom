package main

import (
	"os"
	"fmt"
	"bufio"
	"regexp"
)

// gather stats on result code from web logs and display them
// takes a single logfile as an argument

func openFile(fileName string) (*os.File, error) {
	// attempt to open the file and get a filehandle
	fileHandle, err := os.Open(fileName)
	return fileHandle, err
}

func checkArgs(argList []string) string {
	// make sure we got a command line argument, and return it as the filename
	if len(argList) != 2 {
		fmt.Fprintf(os.Stderr, "must supply file to parse as only argument\n")
		os.Exit(1)
	}
	return argList[1]
}

func scanFile(fileHandle *os.File) map[string]int {
	// do regex matching on the file, one line at a time
	// this is so we can handle arbitrarily large files.

	// this is the regexp we will match on
	re := regexp.MustCompile(` (\d\d\d) `)

	// we will need a map to hold the result codes
	resultCodes := make(map[string]int)

	// read each line of the file to find matches
	fileScanner := bufio.NewScanner(fileHandle)
	for fileScanner.Scan(){
		match := re.FindStringSubmatch(fileScanner.Text())
		resultCodes[match[1]]++
	}

	return resultCodes
}

func countCodes(resultCodes map[string]int) int {
	// go through all the result codes to get the total count
	totalCodeCount := 0

	for _, codeCount := range resultCodes {
		totalCodeCount += codeCount
	}
	fmt.Printf("total codes found: %d\n", totalCodeCount)
	return totalCodeCount
}

func calcPercentages(totalCodeCount int, resultCodes map[string]int) {
	// do some very fancy math to get percentages for each return code
	for resultCode, codeCount:= range resultCodes {
		codePercent := (float64(codeCount) / float64(totalCodeCount)) * 100
		fmt.Printf("code: %s percentage: %.2f\n", resultCode, codePercent)
	}
}

func main(){

	// parse command line and open the file
	fileHandle, err := openFile(checkArgs(os.Args))
	if err != nil {
		fmt.Fprintf(os.Stderr, "error opening file: %s\n", err)
		os.Exit(1)
	}
	defer fileHandle.Close()

	resultCodes := scanFile(fileHandle)

	totalCodeCount := countCodes(resultCodes)

	calcPercentages(totalCodeCount, resultCodes)
}
