package main

import (
	"testing"
	"fmt"
	"os"
)

func TestOpenValidFile(t *testing.T) {
	// test that opening a valid file succeeds and returns a filehandle
	fileName := "loggy-data.txt"

	fileHandle, err := openFile(fileName)
	if err != nil {
		t.Logf("error attempting to open file: %v", err)
		t.Fatal()
	}
}

/*
func TestOpenInvalidFile(t *testing.T){
	// test that opening a file that doesn't exist fails
	fileName := "loggy-invalid.txt"

	// how do I wrap the call to openFile so I can catch the failure?
	openFile(fileName)
}
*/

func TestScanValidFile(t *testing.T) {
	// test that we can process a valid input file
	fileName := "loggy-data.txt"
	fileHandle := openFile(fileName)

	resultCodes := scanFile(fileHandle)

	if resultCodes["200"] > 0 {
		fmt.Println("processed valid input file")
	} else {
		fmt.Println("failed to process valid input file")
		os.Exit(1)
	}
}

func TestCheckArgs(t *testing.T) {
	// test that we can take a filename on the command line
	argList := []string{"dummy", "loggy-data.txt"}

	fileName := checkArgs(argList)

	if fileName == "loggy-data.txt" {
		fmt.Println("got filename")
	} else {
		fmt.Println("failed to get filename!")
		os.Exit(1)
	}
}

func TestCountCodes(t *testing.T) {
	// test that we return a valid count

	resultCodes := map[string]int{
		"200": 5,
		"503": 10,
	}

	if countCodes(resultCodes) != 15 {
		fmt.Println("failed to total result codes!")
	} else {
		fmt.Println("counted result codes correctly")
	}
}

func TestCalcPercentages(t *testing.T) {
	// test that we calculate percentages correctly

	totalCodeCount := 3
	resultCodes := map[string]int{
		"200": 2,
		"503": 1,
	}

	calcPercentages(totalCodeCount, resultCodes)
}
