// To execute Go code, please declare a func main() in a package "main"


/*

Input: `"/foo"` 

    - /foo
      - /images
        - /foo.png  <------.
      - /temp              | same file contents
        - /baz             |
          - /that.foo  <---|--.
      - /bar.png  <--------'  |
      - /file.tmp  <----------| same file contents
      - /other.temp  <--------'
      - /blah.txt

Output:
 [
     ['/foo/bar.png', '/foo/images/foo.png'],
     ['/foo/file.tmp', '/foo/other.temp', '/foo/temp/baz/that.foo']
  ]
  
func findDupes(path string) [][]string {

}
*/


package main

import "fmt"

type FileMap map[int64][]string

fileMap := new(FileMap)


// type Hasher struct {}
// func (h *Hasher) Write([]byte) error
// func (h *Hasher) Sum() int64

func incCheckFile(path string info os.FileInfo, err error) error) {
  var hasher Hasher
  
  for {
    block := ioutil.Read(256)
    err = hasher.Write(block)
    if err != nil {
      break
    }
  }
  fileSha := hasher.Sum()
  
  fileMap[fileSha] = append(fileMap[fileSha], path)
  
  return nil
}  
  
func checkFile(path string info os.FileInfo, err error) error) {
  data, err := ioutil.ReadFile(path)
  if err != nil {
    return err
  }
  fileSha := sha256.Sum256(data)
  fileMap[fileSha] = append(fileMap[fileSha], path)
  
  return nil
}

// func listDir(path string) []string
// func isDir(path string) bool

func myWalk(path string, checkFile func) error {
  numDirs := 0
  
  dirEntries := listDir(path)
  for _, entry := range dirEntries {
    if isDir(entry) {
      err := myWalk(entry, checkFile)
    }
  }
}
            
func findDupes(path string) {  
  err := filepath.Walk(path, checkFile())
  if err != nil {
    panic(err)
  }
}  
                    
func printDupes() {
  for k, v := range fileMap {
    if len(v) == 1 {
      continue
    }
    fmt.Println(v)
  }
}
                      
func main() {
  path = "/foo"
  findDupes(path)
  
  printDupes()
}
