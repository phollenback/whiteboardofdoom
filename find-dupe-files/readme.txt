I believe that this was a dropbox question, but it could have been a pure storage
question as well.  I was also asked to whiteboard something very similar at
fleetsmith.

My general advice is to be comfortable with:

using go's os.Walk to walk directories
writing your own os.Walk equivalent (both iterative and recursive versions)
