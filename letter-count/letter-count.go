package main

// not sure which company this was for - probably doordash

import (
	"fmt"
	"os"
)

// count the letter frequencies in a list of words

func countStuff(words []string) map[rune]int {

	counts := make(map[rune]int)

	// loop over the words
	for _, word := range words {
		// loop over the characters in each word
		for _, char := range word {
			counts[char]++
		}
	}
	return counts
}

func main() {
	words := os.Args[1:]

	counts := countStuff(words)

	fmt.Print("input words: ")
	for _, word := range words {
		fmt.Print(word)
		fmt.Print(" ")
	}
	fmt.Println()
	fmt.Print("letter frequencies: ")
	for key, char := range counts {
		fmt.Printf("%c:", key)
		fmt.Print(char)
		fmt.Print(" ")
	}
	fmt.Println()
}
