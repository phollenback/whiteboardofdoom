package main

// I forget which company asked this in a coderpad test, but it was
// definitely a coderpad test in an interview.

// in general this type of question came up a lot

import "fmt"

func countStuff(testStrings []string) {
  var frequency = make(map[string]int)
  for _, testString := range testStrings {
      for _, char := range testString{
      frequency[string(char)] += 1
      }
  }
  fmt.Println(frequency)  
}

func main() {
  var testStrings []string
  
  testStrings = append(testStrings, "hello")
  testStrings = append(testStrings, "wow")
  
  countStuff(testStrings)
}


/* 
Your previous Plain Text content is preserved below:

frequency : []str -> HashMap<char><int>
frequency(["hello", "wow"]) -> { h: 1, e: 1, l: 2, o: 2, w: 2 }
 */
