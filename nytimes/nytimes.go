package main

// amgen coderpad test

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"time"
)

type ApiResult struct {
	Copyright   string `json:"copyright"`
	LastUpdated string `json:"last_updated"`
	NumResults  int    `json:"num_results"`
	Results     []struct {
		Abstract          string   `json:"abstract"`
		Byline            string   `json:"byline"`
		CreatedDate       string   `json:"created_date"`
		DesFacet          []string `json:"des_facet"`
		GeoFacet          []string `json:"geo_facet"`
		ItemType          string   `json:"item_type"`
		Kicker            string   `json:"kicker"`
		MaterialTypeFacet string   `json:"material_type_facet"`
		Multimedia        []struct {
			Caption   string `json:"caption"`
			Copyright string `json:"copyright"`
			Format    string `json:"format"`
			Height    int    `json:"height"`
			Subtype   string `json:"subtype"`
			Type      string `json:"type"`
			URL       string `json:"url"`
			Width     int    `json:"width"`
		} `json:"multimedia"`
		OrgFacet      []string `json:"org_facet"`
		PerFacet      []string `json:"per_facet"`
		PublishedDate string   `json:"published_date"`
		Section       string   `json:"section"`
		ShortURL      string   `json:"short_url"`
		Subsection    string   `json:"subsection"`
		Title         string   `json:"title"`
		URL           string   `json:"url"`
		UpdatedDate   string   `json:"updated_date"`
	} `json:"results"`
	Section string `json:"section"`
	Status  string `json:"status"`
}

const apiKey = "KPiwXaqV8wKh0hU2FkIxS7gOnRk9C794"
const apiUrl = "https://api.nytimes.com/svc/topstories/v2/world.json?api-key="

func getWorldArticles() ApiResult {
	var apiResult ApiResult

	res, err := http.Get(apiUrl + apiKey)
	if err != nil {
		panic("api call failed!")
	}
	data, err := ioutil.ReadAll(res.Body)

	err = json.Unmarshal(data, &apiResult)
	if err != nil {
		panic("json unmarshal failed")
	}
	return apiResult
}

func processArticles(res ApiResult) {
	dateLayout := "2019-08-06T11:09:56-04:00"

	if res.Section != "world" {
		panic("Section not world!")
	}
	fmt.Print("number of articles: ")
	fmt.Println(len(res.Results))

	for _, r := range res.Results {
		fmt.Println(r.Title)
		createdDate := r.CreatedDate

		createdTime, err := time.Parse(dateLayout, createdDate)
		if err != nil {
			panic("failed to parse createdDate")
		}
		updatedDate := r.UpdatedDate
		updatedTime, err := time.Parse(dateLayout, updatedDate)
		if err != nil {
			panic("failed to parse updatedDate")
		}
		fmt.Print(createdTime)
		fmt.Print(" ")
		fmt.Println(updatedTime)
	}
}

func main() {
	res := getWorldArticles()
	processArticles(res)
}
