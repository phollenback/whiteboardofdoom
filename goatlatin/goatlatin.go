package main

// this was one of the two faebook coderpard tests

import (
	"fmt"
	"strings"
)

/*
# Goat Latin is a made-up language based off of English, sort of like Pig Latin.
# The rules of Goat Latin are as follows:
# 1. If a word begins with a consonant (i.e. not a vowel), remove the first
#    letter and append it to the end, then add 'ma'.
#    For example, the word 'goat' becomes 'oatgma'.
# 2. If a word begins with a vowel (a, e, i, o, or u), append 'ma' to the end of the word.
#    For example, the word 'I' becomes 'Ima'.
# 3. Add one letter "a" to the end of each word per its word index in the
#    sentence, starting with 1. That is, the first word gets "a" added to the
#    end, the second word gets "aa" added to the end, the third word in the
#    sentence gets "aaa" added to the end, and so on.

# Write a function that, given a string of words making up one sentence, returns
# that sentence in Goat Latin. For example:
#
#  string_to_goat_latin('I speak Goat Latin')
#
# would return: 'Imaa peaksmaaa oatGmaaaa atinLmaaaaa'
*/

func contains(letter rune, vowels []rune) bool {
	for _, char := range vowels {
		if letter == char {
			return true
		}
	}
	return false
}

func makeGoatLatin(myString string) string {
	var resultString string

	newString := strings.Split(myString, " ")

	var vowels = []rune{'a', 'e', 'i', 'o', 'u', 'A', 'E', 'I', 'O', 'U'}

	for idx, word := range newString {
		var newWord string
		firstLetter := rune(word[0])
		if contains(firstLetter, vowels) {
			newWord = word + "ma"
		} else {
			wordSlice := strings.Split(word, "")
			newWordSlice := append(wordSlice[1:], wordSlice[0])
			newWord = strings.Join(newWordSlice, "")
			newWord += "ma"
		}
		for idx2 := 0; idx2 <= idx; idx2++ {
			newWord += "a"
		}
		newWord += " "
		resultString += newWord
	}

	strings.TrimSpace(resultString)
	return resultString
}

func main() {
	originalString := "I speak Goat Latin"
	resultString := makeGoatLatin(originalString)
	fmt.Println(originalString)
	fmt.Println(resultString)
}
