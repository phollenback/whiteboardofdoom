package main

// this was a dropbox coderpad test

import (
	"fmt"
	"strings"
)

// need to implement this in goroutines

var sentences = []string{"The driver of a cable car is known as the gripman or grip operator",
	"This is a highly skilled job that requires the gripman to juggles three separate tasks",
	"The driver must smoothly operate the grip lever to grip and release the cable",
	"They must release the grip at certain points to coast the vehicle over crossing cables or places where the cable does not follow the tracks",
	"The driver must anticipate well in advance possible collisions with other traffic that may not understand the limitations of a cable car",
	"Being a gripman requires great upper body strength needed for the grip and brakes as well as good hand eye coordination and balance",
	"As of August last year there have been two gripwomen Fannie Mae Barnes who served for four years and Willa Johnson who is still working",
	"Besides the gripman each cable car carries a conductor whose job is to collect fares manage the boarding and exiting of passengers and control the rear wheel brakes when descending hills",
	"With the common practice of carrying standing passengers on the running boards of cable cars passenger management is also an important task",
	"Some crew members are locally well known personalities",
}

var stopWords = []string{"I", "the", "is", "as", "a", "of", "or", "in", "are", "on", "of", "not", "besides", "this", "they", "with", "some", "and"}

/*
# TODO: Complete the implementation of the summarizer

class Summarizer:
    def __init__(self, sentences: list, stop_words: list):
	pass

    # Result dict has keys composed of words that are all lowercase
    # values are integers of how many were found of each word
    # stop words are ignored
    def summary(self, min_length = 3) -> dict:
	pass
*/

func notStopWord(word string) bool {
	for _, stopWord := range stopWords {
		if word == stopWord {
			return false
		}
	}
	return true
}

func Summarizer(sentences []string, stopList []string) map[string]int {
	words := []string{}
	wordsMap := make(map[string]int)

	for _, sentence := range sentences {
		words = strings.Split(sentence, " ")
		for _, word := range words {
			lowerWord := strings.ToLower(word)
			if notStopWord(lowerWord) {
				wordsMap[lowerWord]++
			}
		}
	}
	return wordsMap
}

func printResults(resultsMap map[string]int) {
	for key := range resultsMap {
		value := resultsMap[key]
		if value > 1 {
			fmt.Print(key)
			fmt.Print(": ")
			fmt.Println(value)
		}
	}
}
func main() {
	resultMap := Summarizer(sentences, stopWords)
	printResults(resultMap)
}
