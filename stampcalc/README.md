This expands on a question I was asked in a coding interview:

Given a set of user login timestamps, determine if the user logged in 5 or more
times in any 30 day period.

The general answer is to sort the list of timestamps from most to least
recent, and then check if timestamp[n] is within 30 days of timestamp[n+4]. If
so, you know that the user logged in 5 times within that 30 day window.  You
can then check timestamp[n+1] against timestamp[n+5], etc - until you get to
the end of the list of timestamps.

You need to be sure to check the degenerate case of less than 5 timestamps.

You also need to be sure to not run off the end of the list.

I've also written some tests to cover some of the basic operations of this
program.