package main

// I think this was the initial postmates coerpad test.
// was followed up by this take home: 
// https://gist.github.com/aabhassharma/774f5bc6e700b168974df3406a5acb68
import (
	"fmt"
	"math/rand"
	"sort"
	"time"
)

// take a series of usernames and epoch timestamps,
// determine if any user has logged in more than 5 times
// in any 30 day window

// initialize the RNG
func init() {
	rand.Seed(time.Now().UnixNano())
}

// generate a random 10 character username
func generateUserName() string {
	var letters = []rune("abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ")

	b := make([]rune, 10)
	for i := range b {
		b[i] = letters[rand.Intn(len(letters))]
	}
	return string(b)
}

// generate a random epoch time within the last year
func generateTimeStamp() int64 {

	now := time.Now()
	unixTime := now.Unix()
	// number of epoch seconds in one year
	var oneYearAgo = unixTime - (60 * 60 * 24 * 365)
	timeRange := unixTime - oneYearAgo
	timeStamp := float64(oneYearAgo) + rand.Float64()*float64(timeRange)

	return int64(timeStamp)
}

// userData holds both the username string and the list of timestamps
type UserData struct {
	userName   string
	timeStamps []int64
}

// create a random lst of usernames
// each username has an arbitrary list of timestamps
// timestamps are in unix epoch format
func generateUserData(userCount int) []UserData {
	var userData UserData
	var usersData []UserData

	// loop through userData userCount times
	// each time, generate a userName
	// then, generate X timestamps for each username
	// X is arbitrarily between 1 and 10
	// the range of timestamps is within the last year

	for i := 0; i < userCount; i++ {
		userData.userName = generateUserName()
		// use Intn, shift up to 1-10
		for j := 0; j < (rand.Intn(1) + 9); j++ {
			userData.timeStamps =
				append(userData.timeStamps, generateTimeStamp())
		}
		usersData = append(usersData, userData)
	}
	return usersData
}

// go through all the users and return list of any who have logged in 5
// times or more in any 30 day window
func findBusyUsers(usersData []UserData) ([]string, bool) {
	var busyUsers []string
	var success = false

	for _, userData := range usersData {
		sortedTimeStamps := sortTimeStamps(userData)
		// check each timestamp
		if checkTimeStamps(sortedTimeStamps) {
			busyUsers = append(busyUsers, userData.userName)
			success = true
		}
	}
	return busyUsers, success
}

// extract list of timestamps from a userData struct, sort them, and return the list
func sortTimeStamps(userData UserData) []int64 {
	stampsLen := len(userData.timeStamps)
	timeStamps := make([]int64, stampsLen)
	copy(timeStamps, userData.timeStamps)
	sort.Slice(timeStamps, func(i, j int) bool {
		return timeStamps[i] > timeStamps[j]
	})

	return timeStamps
}

// check a list of presorted timestamps to determine if 5 of them are
// in a 30 day window.
func checkTimeStamps(sortedTimeStamps []int64) bool {

	// 30 days, expressed in seconds
	const thirtyDays = (60 * 60 * 24 * 30)

	// return immediately if given less than 5 timestamps
	if len(sortedTimeStamps) < 5 {
		return false
	}

	for i, timeStamp := range sortedTimeStamps {
		// make sure we don't run off the end
		if i > len(sortedTimeStamps)-5 {
			return false
		}
		if timeStamp-sortedTimeStamps[i+4] < thirtyDays {
			// timestamp and 5th timestamp are within 30 days
			// thus, 5 timestamps are within 30 days
			return true
		}
	}
	// we fell through, no set of 5 timestamps are within 30 days
	return false
}

func main() {

	fmt.Println("Generating fake user names and timestamps for 10 users")
	usersData := generateUserData(10)
	busyUsers, _ := findBusyUsers(usersData)
	fmt.Println("Following users have 5 or more timestamps within 30 day window:")
	for _, userName := range busyUsers {
		fmt.Println(userName)
	}
}
