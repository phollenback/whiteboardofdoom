package main

import (
	"fmt"
	"testing"
	"time"
)

// verify we can generate a username
func TestGenUserName(t *testing.T) {
	var userName string

	userName = generateUserName()
	if len(userName) >= 1 {
		fmt.Println("generated username: " + userName)
	} else {
		t.Fail()
	}
}

// verify we can generate a timestamp
func TestGenTimeStamp(t *testing.T) {
	var timeStamp int64
	now := time.Now()
	unixTime := now.Unix()

	timeStamp = generateTimeStamp()
	if timeStamp > 0 && timeStamp <= unixTime {
		fmt.Printf("generated timestamp: %d\n", timeStamp)
	} else {
		t.Fail()
	}
}

// verify we can generate random data for one user
func TestGenUserData(t *testing.T) {
	var usersData []UserData
	usersData = generateUserData(1)

	if len(usersData[0].userName) > 0 {
		fmt.Println("successfully generated userdata struct")
	} else {
		t.Fail()
	}
}

// For testing whether we find busy users (users with 5 or more
// timestamps in 30 days) I should create a helper function that sets
// up a usersData array.  The below results in a lot of duplicated
// code.

// verify that we correctly sort timestamps
func TestSortTimeStamps(t *testing.T) {
	timeStamps := []int64{1, 5, 11, 2, 42}
	resultTimeStamps := []int64{42, 11, 5, 2, 1}

	var userData = UserData{
		timeStamps: timeStamps,
		userName:   "TestBob2",
	}

	sortedTimeStamps := sortTimeStamps(userData)

	for i, v := range sortedTimeStamps {
		if v != resultTimeStamps[i] {
			t.Log("timestamps not sorted correctly!")
			t.FailNow()
		}
	}
	fmt.Println("successfully tested sorting timestamps")
}

// verify that a degenerate example (one userData with one timestamp)
// results in no busy users
func TestDegenFindBusyUsers(t *testing.T) {
	var timeStamps []int64
	var usersData = make([]UserData, 1)

	timeStamps = append(timeStamps, 0)

	var userData = UserData{
		timeStamps: timeStamps,
		userName:   "TestBob1",
	}

	usersData = append(usersData, userData)
	_, success := findBusyUsers(usersData)
	if success {
		t.Log("should not have found any busy users")
		t.Fail()
	} else {
		fmt.Println("successfully tested degenerate userdata case")
	}
}

// create a minimal usersData that should pass (one user, 5 timestamps
// the same)
func TestBusyUsersShouldPass(t *testing.T) {
	var usersData = make([]UserData, 1)

	timeStamps := []int64{1, 2, 3, 4, 5}

	var userData = UserData{
		timeStamps: timeStamps,
		userName:   "TestBob1",
	}
	usersData = append(usersData, userData)
	_, success := findBusyUsers(usersData)
	if success {
		fmt.Println("5 adjacent timestamps passed")
	} else {
		t.Log("5 adjacent timestamps - should have passed")
		t.Fail()
	}
}

// test one user with 4 adjacent timestamps and one more than 30 days
// apart.
func TestBusyUsersShouldFail(t *testing.T) {
	var usersData = make([]UserData, 1)

	timeStamps := []int64{1, 2, 3, 4, 3000000}

	var userData = UserData{
		timeStamps: timeStamps,
		userName:   "TestBob1",
	}
	usersData = append(usersData, userData)
	_, success := findBusyUsers(usersData)
	if !success {
		fmt.Println("4 adjacent timestamps and one outside 30 days passed")
	} else {
		t.Log("1 of 5 timestamps outside 30 days should not have succeeded")
		t.Fail()
	}
}
